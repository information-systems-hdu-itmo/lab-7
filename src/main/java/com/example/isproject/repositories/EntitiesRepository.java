package com.example.isproject.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.isproject.entities.DBEntityInfo;
import java.util.List;
import java.util.Optional;



@Repository
public interface EntitiesRepository extends JpaRepository<DBEntityInfo, Long> {
    List<DBEntityInfo> findByValue(String value);

    @Query(value="SELECT * FROM entities e WHERE e.value LIKE CONCAT('%', :paramValue, '%')", nativeQuery = true)
    List<DBEntityInfo> findByValueContaining(String paramValue);
    
}
