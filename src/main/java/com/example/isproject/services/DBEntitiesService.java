package com.example.isproject.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.isproject.entities.DBEntityInfo;
import com.example.isproject.repositories.EntitiesRepository;

@Service
public class DBEntitiesService {
	
    private final EntitiesRepository entitiesRepository;

    public DBEntitiesService(EntitiesRepository entitiesRepository) {
    	this.entitiesRepository = entitiesRepository;
    }

	public List<DBEntityInfo> getEntities() {
		return entitiesRepository.findAll();
	}
	
	public Optional<DBEntityInfo> getEntities(long id) {
		return entitiesRepository.findById(id);
	}
	
	public DBEntityInfo createNewEntity(String value) {
		DBEntityInfo e = new DBEntityInfo(null, value);
		return entitiesRepository.save(e);
	}

	public List<DBEntityInfo> getEntitiesByValue(String value) {
		return entitiesRepository.findByValue(value);
	}

	public List<DBEntityInfo> getEntitiesContainingValue(String value) {
		return entitiesRepository.findByValueContaining(value);
	}

}
