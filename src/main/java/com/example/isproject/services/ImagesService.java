package com.example.isproject.services;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.stream.StreamSupport;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.example.isproject.access.ImageData;
import com.example.isproject.entities.DBEntityInfo;


@Service
public class ImagesService {
	
	private final DBEntitiesService entitiesService;
	private final Path uploadRootPath;
	
	
	public ImagesService(DBEntitiesService entitiesService) {
		this.entitiesService = entitiesService;
		this.uploadRootPath = this.prepareUploadRootPath(System.getenv("UPLOAD_LOCATION"));
	}
	
	private Path prepareUploadRootPath(String pathString) {
		if (pathString != null) {
			  Path path = Path.of(pathString);
			if(Files.isDirectory(path)) {
				return path;
			}
		}
		
		throw new RuntimeException("Images upload location is not specified! Check the UPLOAD_LOCATION environment variable.");  
	}


	public DBEntityInfo createImage(String value, String fileName, InputStream data) throws IOException {
		DBEntityInfo entity = this.entitiesService.createNewEntity(value);
		Path localFilePath = uploadRootPath.resolve(entity.getId() + extractFileNameExtension(fileName));
		Files.copy(data, localFilePath);
		return entity;
		
	}

	private String extractFileNameExtension(String name) {
		String ext = ".";
		if(name != null) {
			int n = name.lastIndexOf('.');
			if(n != -1) {
				ext = name.substring(n).replaceAll("[^a-zA-Z0-9-_.]", "_");
			}
		}
		return ext;
	}

	public Optional<ImageData> findById(long id) throws IOException {
		Optional<DBEntityInfo> info = entitiesService.getEntities(id);
		if (info.isPresent()) {
			try (DirectoryStream<Path> fs = Files.newDirectoryStream(uploadRootPath, id + ".*")) {
				Optional<Path> file = StreamSupport.stream(fs.spliterator(), false).findFirst();
				if (file.isPresent()) {
					Path path = file.get();
					String contentType = Files.probeContentType(path);
					MediaType mediaType = contentType == null ? null : MediaType.parseMediaType(contentType);
					String valueBasedFileName = info.get().getValue() + extractFileNameExtension(path.toFile().getName());
					byte[] rawData = Files.readAllBytes(path);
					return Optional.of(new ImageData(rawData, valueBasedFileName, mediaType, info.get()));
					
				}
			}
		}
		return Optional.empty();
	}
}
