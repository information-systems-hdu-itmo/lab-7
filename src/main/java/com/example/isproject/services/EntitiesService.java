package com.example.isproject.services;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.example.isproject.access.EntityInfo;

@Service
public class EntitiesService {
	
    private final ArrayList<EntityInfo> entities = new ArrayList<>();

    public EntitiesService() {
    	Random rnd = new Random();
    	byte[] nums = new byte[10];
    	
    	for (int i = 0; i < 25; i++) {
    		rnd.nextBytes(nums);
    		entities.add(new EntityInfo(i, Base64.getEncoder().encodeToString(nums)));
    	}
    }

	public List<EntityInfo> getEntities() {
		return entities;
	}
}
