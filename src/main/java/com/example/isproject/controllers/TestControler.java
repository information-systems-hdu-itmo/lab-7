package com.example.isproject.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.example.isproject.services.DBEntitiesService;
import com.example.isproject.services.ImagesService;
import com.example.isproject.access.EntityInfo;
import com.example.isproject.access.ImageData;
import com.example.isproject.entities.DBEntityInfo;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@RestController
@RequestMapping("/")
public class TestControler {

	private final DBEntitiesService service;
	private final ImagesService imagesService;

	public TestControler(DBEntitiesService service, ImagesService imagesService) {
		this.service = service;
		this.imagesService = imagesService;
	}
	
	@GetMapping(value = "/test", params = {"value"})
	public List<EntityInfo> getEntityByExactValue(@RequestParam String value) {
		return mapCollection(service.getEntitiesByValue(value));
	}

	@GetMapping(value = "/test", params = {"valueLike"})
	public List<EntityInfo> getEntityByLikeValue(@RequestParam String valueLike) {
		return mapCollection(service.getEntitiesContainingValue(valueLike));
	}


	@GetMapping("/test")
	public List<EntityInfo> getEntities() {
		var data = service.getEntities();
		return mapCollection(data);
	}

	
	@PostMapping("/test")
	public EntityInfo createNewEntity(@RequestParam String value) {
		DBEntityInfo info = service.createNewEntity(value);
        return new EntityInfo(info.getId(), info.getValue());
	}
	
	private EntityInfo mapItem(DBEntityInfo info) {
		return new EntityInfo(info.getId(), info.getValue());
	}
	
	@PostMapping(path = "/test", params = {"imageName"})
	public EntityInfo createNewEntityWithImage(@RequestParam String imageName, @RequestParam("file") MultipartFile file) throws IOException {
		try (InputStream inputFileData = file.getInputStream()) {
			DBEntityInfo info = imagesService.createImage(imageName, file.getOriginalFilename(), inputFileData);
			return mapItem(info);
		}
	}
	
	@GetMapping(path = "/imgs/{id}")
	public ResponseEntity<byte[]> getImage(@PathVariable long id) throws IOException {
		Optional<ImageData> result = imagesService.findById(id);
		if (result.isPresent()) {
			ImageData data = result.get();
			return ResponseEntity.status(HttpStatus.OK)
				.contentType(data.mediaType)
				.body(data.rawData);
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}
	
	private List<EntityInfo> mapCollection(List<DBEntityInfo> data) {
		ArrayList<EntityInfo> result = new ArrayList<EntityInfo>();
		for (DBEntityInfo entity : data) {
			result.add(new EntityInfo(entity.getId(), entity.getValue()));
		}
		return result;
	}
}
