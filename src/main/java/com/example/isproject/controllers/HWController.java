package com.example.isproject.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/isproject")
public class HWController {
    
	@Value("${application.message:Hello World}")
    private String message;
	
	@Autowired
	private MyUserContext context;
	
	@Bean
	@SessionScope
	public MyUserContext getContext() {
	    return new MyUserContext();
	}

	@PostMapping(value = "/hello", consumes = { "multipart/form-data" })
	public ResponseEntity<Object> postHelloName(
	    @RequestParam(value = "name", required = true, defaultValue="") String name
	) {
	  context.setName(name);
	  return ResponseEntity.status(HttpStatus.FOUND).header("Location", "/isproject/hello").build();
	}
	
	@GetMapping("/hello")
	public ModelAndView getHelloName() {
	    return new ModelAndView("hello-view", "username", context.getName());
	}
	
	@GetMapping("/hw")
	public ModelAndView getHello(Map<String, Object> model) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("helloworld");
		mv.getModel().put("message", message);
		return mv;
	}

}
