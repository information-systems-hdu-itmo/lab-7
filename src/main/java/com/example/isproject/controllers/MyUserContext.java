package com.example.isproject.controllers;

public class MyUserContext {
    public String name;
    
    public void setName(String name) {
    	this.name = name;
    }
    
    public String getName() {
    	return name;
    }
}
