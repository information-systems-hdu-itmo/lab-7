package com.example.isproject.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="entities")
public class DBEntityInfo {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name="value")
	private String value;
	
	public DBEntityInfo() {
		
	}

	public DBEntityInfo(Long id, String value) {
		this.id = id;
		this.value = value;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}

}
