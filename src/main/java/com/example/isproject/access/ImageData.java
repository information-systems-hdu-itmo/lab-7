package com.example.isproject.access;

import org.springframework.http.MediaType;

import com.example.isproject.entities.DBEntityInfo;

public class ImageData {
    public byte[] rawData;
    public String fileName;
    public MediaType mediaType;
    public DBEntityInfo entity;
    
	public ImageData(byte[] rawData, String fileName, MediaType mediaType, DBEntityInfo entity) {
		this.rawData = rawData;
		this.fileName = fileName;
		this.mediaType = mediaType;
		this.entity = entity;
	}
}
