package com.example.isproject.access;


public class EntityInfo {
    private final long id;
    private final String value;
    
	public EntityInfo(long id, String value) {
		this.id = id;
		this.value = value;
	}
	
	public long getId() {
		return id;
	}
	
	public String getValue() {
		return value;
	}
}
